# Our user must have rights to the "docker" group.

if ! echo "$(groups)" | grep -q docker
then
    sudo adduser $USER docker
    exec newgrp docker < ./build.sh
fi

docker build -t mikes_development_machine --force-rm=true .
