# README #

This is mikes development machine.

### What is this repository for? ###

* Bring up a common dev box on multiple OS's
* 1.0.0

### How do I get set up? ###

* Requires Docker version 1.12 minimum version
* Requires internet connection to docker hub for pull the fedora:latest image
* run bootstrap.sh script if on linux
* if on windows the run docker build -t devbox . to build the docker container

### Who do I talk to? ###

* Michael J Reid <pershing2.crr@gmail.com>
