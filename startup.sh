#!/bin/bash

set -e -x

# If no other commands get appended, then we
# arrange for this shell to stay alive forever instead of exiting,
# because exiting would terminate the container.

sleep_for_ten_years () {
    sleep 315360000
}

if tail -1 "$0" | grep -q /etc/init.d/sshd
then
 trap sleep_for_ten_years EXIT
fi

