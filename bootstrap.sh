#!/bin/bash

set -e

cd ${PWD}

if [ ! 'sudo ip netns' ]; then
    echo "ip netns not supported, installing fix"
    tar xf iproute2-mreid.tar.gz; make && make install
else
    echo "ip netns supported"
fi

echo "-----------------------------------------------------------------------"
echo "building system"
echo "-----------------------------------------------------------------------"
/bin/bash build.sh
echo "-----------------------------------------------------------------------"
echo "building system complete "
echo "-----------------------------------------------------------------------"

echo "-----------------------------------------------------------------------"
echo "bootstrap script complete; Next setup run launch.sh "
echo "-----------------------------------------------------------------------"

