FROM fedora:latest
MAINTAINER Michael J. Reid <pershing2.crr@gmail.com>
LABEL Description="Mikes Dev Box"

RUN dnf update -y; dnf clean all
RUN dnf install vim \
                @development-tools \
                openssh-clients \
                openssh-server \
                cmake \
                valgrind -y; dnf clean all

# Allow login to servers via SSH, preventing (in auth.log):
# Cannot open /proc/self/loginuid: Read-only file system
# set_loginuid failed
RUN sed -i /loginuid/s/^/#/ /etc/pam.d/sshd

RUN echo 'root:hero21' | chpasswd
RUN sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config
RUN echo 'UseDNS no' >> /etc/ssh/sshd_config
RUN echo 'UserKnownHostsFile /dev/null' >> /etc/ssh/ssh_config
RUN echo 'StrictHostKeyChecking no' >> /etc/ssh/ssh_config

# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

# Support SSH between hosts without a password.
RUN mkdir -p   /root/.ssh
ADD id_rsa     /root/.ssh/id_rsa
ADD id_rsa.pub /root/.ssh/id_rsa.pub
ADD id_rsa.pub /root/.ssh/authorized_keys
RUN chmod -R og-rwx /root/.ssh

RUN echo "export VISIBLE=now" >> /etc/profile

# setup sshd keys.
RUN service sshd start
RUN service sshd stop
ADD startup.sh /startup.sh

CMD ["/startup.sh"]
CMD ["/usr/sbin/sshd", "-D"]

